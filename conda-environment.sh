# Create new python3 conda environment "diffmer" with required packages

conda create --name diffmer  python=3  pip numpy numba matplotlib pandas hdf5 h5py seaborn

# To activate the environment
source activate diffmer

# install additional packages (not supported by conda) with pip
pip install snakemake    # for running workflows

# Finally, run setup.py to install the package
# To do this in development mode (so repository updates become immediately active), run:
# python setup.py develop

# Otherwise, run 
python setup.py build install

# To remove:
# conda remove --name diffmer --all

