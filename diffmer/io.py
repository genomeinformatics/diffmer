# diffmer.io module

import configparser
import sys
import numpy as np
import pandas as pd
import h5py
from collections import OrderedDict
import numpy.ma as ma


def read_bedfile(path, samplenames, cases):
    """
    Read methylation information from a BED file.
    path: path to BED file
    samplenames: list of sample names, corresponding to order in the BED file

    Return a DataFrame, where:
    - each row corresponds to a CpG,
      (multi-indexed for chromosome and start position)
    - each pair of columns to a sample
      (multi-indexed for sample and M, U+M).
    """

    typ = ["M", "U+M"]
    # multiind = pd.MultiIndex.from_product([samplenames, typ],
    #                                       names=["sample", "meth"])
    # ## case and control are accessable via data["case"] and data["control"]
    multiind = pd.MultiIndex.from_tuples(
        [("case" if s in cases else "control", t, s)
         for s in samplenames for t in typ],
        names=["class", "sample", "meth"])

    df = pd.read_csv(path, delim_whitespace=True, skiprows=0, header=None,
                     names=multiind, index_col=[0, 1, 2],
                     dtype={s: np.int32 for s in multiind})
    df.index.names = ["chr", "pos", "stop"]
    return df


def iterate_bedfile_chromosomewise(path, samplenames, cases):
    """
    Read methylation information from a BED file.
    path: path to BED file
    samplenames: list of sample names, corresponding to order in the BED file

    Return a TextFileReader, where:
    - each row corresponds to a CpG,
      (multi-indexed for chromosome and start position)
    - each pair of columns to a sample
      (multi-indexed for sample and M, U+M).
    """
    typ = ["M", "U+M"]
    multiind = pd.MultiIndex.from_tuples(
        [("case" if s in cases else "control", t, s)
         for s in samplenames for t in typ],
        names=["class", "meth", "sample"])

    df = pd.read_csv(*path, delim_whitespace=True, skiprows=0, header=None,
                     names=multiind, index_col=[0, 1, 2],
                     dtype={s: np.uint16 for s in multiind}, chunksize=50000)
    # TODO: stop position raus nehmen

    p = Lookahead(df)
    del df
    chunk = p.next()
    chromosomes = list(OrderedDict.fromkeys(chunk.index.get_level_values(0)))
    akt_chrom = chromosomes[0]
    parts = []

    while (True):
        try:
            grouped = chunk.groupby(level=0)
            while len(chromosomes) > 1:
                parts.append(grouped.get_group(akt_chrom))
                yield pd.concat(parts)
                parts = []
                # update
                chromosomes.pop(0)
                akt_chrom = chromosomes[0]

            # only 1 chromosome
            parts.append(grouped.get_group(akt_chrom))
            # check next chunk
            peek = p.peek()
            if peek is None:  # EOF
                yield pd.concat(parts)
                return
            # else: get next chunk
            chromosomes = list(OrderedDict.fromkeys(
                peek.index.get_level_values(0)))
            # current chromosome ends at chunk end
            if akt_chrom not in chromosomes:
                yield pd.concat(parts)
                parts = []
            akt_chrom = chromosomes[0]
            chunk = p.next()
        except StopIteration:
            return


def read_hdf5file(files, index_file, samplenames, cases):
    """
    Yields a DataFrame per chromosome of the given data
    files: Multiple hdf5 filenames, each contains methylation information
           for a single sample
    index: file with position-index
    samplenames: list of samplenames, must be same order as in files
    cases: list of case-samplenames
    """
    if index_file is None:
        raise RuntimeError("no index file given for hdf5 input")
    typ = ["M", "U+M"]
    multiind = pd.MultiIndex.from_tuples(
        [("case" if s in cases else "control", t, s)
         for s in samplenames for t in typ],
        names=["class", "sample", "meth"])

    file = h5py.File(files[0], "r")
    chromosomes = list(file.keys())
    # determine Nome or WGBS:

    if "nome" in file[chromosomes[0]].keys():
        val_type = "nome"
        pos_type = "gpc_positions"
    else:
        val_type = "methylation"
        pos_type = "cpg_positions"

    file.close()
    ix_file = h5py.File(index_file[0], "r")

    for chrom in chromosomes:
        data = []
        # two datasets: methylation and nome
        if chrom.startswith("GL"):
            continue
        try:
            for filename in files:
                with h5py.File(filename, "r")as f:
                    values = f[chrom][val_type][:]
                    coverage = np.sum(values, axis=1)
                    methylation = values[:, 0] + values[:, 2]
                    data.extend([methylation, coverage])

            cor_data = np.vstack(data).transpose()
            positions = ix_file[chrom][pos_type][:]
            # mask all invalid positions
            # mx = ma.masked_array(x, mask = [0,0,1,1,0])
            # mx[~xm.mask].data

            index = pd.MultiIndex.from_product(["chr"+chrom, positions],
                                               names=["chr", "pos"])
            df = pd.DataFrame(cor_data,  index=index,
                              columns=multiind, dtype=np.uint16)

            yield df
        except GeneratorExit:
            return
        except KeyError:
            print("error handling chromosome {}. No Index found. Skipping".
                  format(chrom), file=sys.stderr)


def read_input_file(path, samplenames, cases, index=None, format=None):
    """
    Read an input file with methylation information
    and return a DataFrame.

    The file must either have the ending .bed oder .hdf5,
    or the format must be explictly specified.

    path: Path including input filename
    samplenames: list of sample names
    format: None or 'bed' or 'hdf5'
    """

    if len(samplenames) == 0:
        raise RuntimeError("no samples given; use --classfile or --classes")
    if format is None:
        if isinstance(path, list):
            if path[0].endswith(".bed"):
                format = "bed"
            elif path[0].endswith((".h5", ".hdf5")):
                format = "hdf5"
        else:
            if path.endswith(".bed"):
                format = "bed"
            elif path.endswith((".h5", ".hdf5")):
                format = "hdf5"
    if format == "bed":
        # return read_bedfile(path, samplenames, cases)
        return iterate_bedfile_chromosomewise(path, samplenames, cases)
    elif format == "hdf5":
        return read_hdf5file(path, index, samplenames, cases)
    else:
        raise ValueError("Unknown format in path '{}': '{}'".
                         format(path, format))


def get_samplenames(classes, classfile):
    """
    Return a pair (cases, controls) of lists of sample names
        for case and control group.
    Assignment must be given by strings of the form samplename=class,
    where class is 0 (control) or 1 (case).

    classes: list of samplename=class strings, or None
    classfile: path to a file with assignments of this form, or None
    """
    if classes is None and classfile is None:
        return([], [], [])
    parser = configparser.ConfigParser(
        empty_lines_in_values=False, interpolation=None)
    parser.optionxform = str  # allow case-sensitive keys
    if classfile is not None:
        with open(classfile) as f:
            c = ["[classes]"] + f.readlines()
        parser.read_file(c)
    if classes is not None:
        parser.read_file(["[classes]"] + classes)
    samplenames = []
    cases = []
    controls = []
    for (samplename, cl) in parser.items("classes"):
        samplenames.append(samplename)
        if cl == "0":
            controls.append(samplename)
        elif cl == "1":
            cases.append(samplename)
        else:
            raise ValueError("Unknown class '{}' for sample '{}'".
                             format(cl, samplename))
    return (samplenames, cases, controls)


def read_classlevels(path):
    """Return estimated class methylation levels (DataFrame)"""
    if path.endswith("h5"):
        return read_hdf5classlevels(path)
    else:
        data = pd.read_csv(path, delim_whitespace=True, comment="#",
                           index_col=[0, 1])
        # data.index.get_level_values('chr').astype(str)
        # df[0] = df[['two', 'three']].astype(float)
        # data.index.names = ["chr", "pos"]
        return data


def read_hdf5classlevels(path):
    """
    TODO: docstring
    """
    # TODO: refactor
    # TODO: force values to be int32

    f = h5py.File(path, "r")
    # df = pd.DataFrame(columns=["case", "control"],
    #                   index = pd.MultiIndex(levels=[[],[]],
    #                   labels=[[],[]],
    #                   names=['chr', 'pos']))
    for chrom in f["positions"]:
        mvalues = np.array([])
        positions = f["positions"][chrom][:]
        # index = pd.MultiIndex.from_product([chrom, positions],
        #                                    names = ["chr", "pos"])
        values = f["classlevels"][chrom][:]
        if mvalues.size == 0:
            mvalues = values
        else:
            mvalues = np.vstack((mvalues, values))
        data = pd.DataFrame(mvalues,  index=positions,
                            columns=["case", "control"])
        yield chrom, data
        # df = df.append(data)
    # return df


def get_pvalue_params(filename):
    try:
        f = h5py.File(filename, "r")
        return f["pValues"].value
    except:
        return None


def read_whole_dmrs(filename):
    if filename is None:
        raise ValueError("No regions to plot")

    with open(filename, "r") as f:
        for line in f:
            if line.startswith("#"):
                continue
            chrom, start, stop, *others = line.split("\t")
            others[-1] = others[-1].strip("\n")
            yield chrom, int(start), int(stop), others


def read_dmrs(filename, manual_region):
    if filename is None and manual_region is None:
        raise ValueError("No regions to plot")
    if filename is not None:
        with open(filename, "r") as f:
            for line in f:
                if line.startswith("#"):
                    continue
                chrom, start, stop, *_ = line.split()  # line.split("\t")
                yield chrom, int(start), int(stop)
    if manual_region is not None:
        for region in manual_region:
            chrom, positions = region.split(":")
            start, stop = [int(x) for x in positions.split("-")]
            yield chrom, start, stop
            # parser = configparser.ConfigParser(
            # empty_lines_in_values=False, interpolation=None)
            # parser.read_file(["[region]"] + manual_region)
            # yield parser["region"]["chr"], int(parser["region"]["start"]), \
            #   int(parser["region"]["stop"])


def get_settings(filename):
    """reads all settings, that is, a comment that contains a "=" and
       stores them in a dictionary"""
    if filename is None:
        raise ValueError("No file given")
    settings = {}
    if filename.endswith("h5"):
        f = h5py.File(filename, "r")
        for key in f["settings"].keys():
            settings[key] = f["settings"][key].value
        return settings

    else:
        with open(filename, "r") as f:
            for line in f:
                if line.startswith("#"):
                    line = line.strip("#")
                    line = line.strip()
                    if "=" in line:
                        values = line.split("=")
                        settings[values[0]] = float(values[1])
                else:
                    return settings


def write_settings(args):
    f = h5py.File(args.output, "w")
    grp = f.create_group("settings")
    # grp.create_dataset("aclass", data = args.aclass)
    grp["aclass"] = args.aclass
    grp["adiff"] = args.adiff
    grp["aspace slope"] = args.slope
    grp["aspace offset"] = args.offset
    grp["aspace scale"] = args.scale
    grp["cut"] = args.cut
    grp["precision"] = args.precision
    return f


class Lookahead:
    def __init__(self, iter):
        self.iter = iter
        self.buffer = []

    def __iter__(self):
        return self

    def next(self):
        if self.buffer:
            return self.buffer.pop(0)
        else:
            return self.iter.get_chunk()

    def peek(self):
        """Return an item n entries ahead in the iteration."""
        while len(self.buffer) < 1:
            try:
                self.buffer.append(self.iter.get_chunk())
            except StopIteration:
                return None
        return self.buffer[0]


if __name__ == "__main__":
    classtxt = "/scratch/HumanCD/data/classes.txt"
    inputbed = "/scratch/HumanCD/data/input.bed"
    (samplenames, cases, controls) = get_samplenames(None, classtxt)
    read_input_file(inputbed, samplenames, cases, None)
