import matplotlib.pyplot as plt
import numpy as np
from matplotlib.font_manager import FontProperties
from . import io
import subprocess
import seaborn as sns
sns.set()
sns.set_context("talk")
sns.set_palette(sns.color_palette("husl", 10), n_colors=10)


def plot_threshold_vs_CpGs(classlevels, path):
    for delta in np.arange(0.1, 1.1, 0.1):
        print(delta)
        create_plot("CpGs", "Minimum Number $S$ of CpGs in one DMR", "Number of CpGs")
        create_plot("DMRs", "Minimum Number $S$ of CpGs in one DMR", "Number of DMRs")
        keys = [4,6,8,10,12,14,16,18,20]

        for t in np.arange(delta, 1.1, 0.1):
            dmrs = {key: 0 for key in keys}
            cpgs = {key: 0 for key in keys}
            
            for S in keys:
                numbers = [int(n) for n in call_dmrs(classlevels, delta, t, S)]
                cpgs[S] = sum(numbers)
                dmrs[S] = len(numbers)
            try:
                plt.figure("DMRs")
                ax = plt.subplot(111)
                ax.set_yscale('log')
                ax.plot(keys, [dmrs[key] for key in keys], label=str(t))
                ax.set_xticks(keys)
            except ValueError:
                print("No DMRs found")

            try:
                plt.figure("CpGs")
                ax = plt.subplot(111)
                ax.set_yscale('log')
                ax.plot(keys, [cpgs[key] for key in keys], label=str(t))
                ax.set_xticks(keys)
            except ValueError:
                print("No DMRs found")
        #ax.plot(list_of_distances_mean, "r", label="Abstand zum Mittelwert")
        plt.figure("DMRs")
        ax = plt.subplot(111)
        set_legend_position(ax, "Threshold $\overline{\Delta}$")
        ax.set_title("$\Delta = {:,.1f}$".format(delta))
        
        if path is None:
            plt.show()
        else:
            plt.savefig(path+"thresholdDMRs_{:,.1f}.pdf".format(delta))
        plt.close()

        plt.figure("CpGs")
        ax = plt.subplot(111)
        set_legend_position(ax, "Threshold $\overline{\Delta}$")
        ax.set_title("$\Delta = {:,.1f}$".format(delta))
        if path is None:
            plt.show()
        else:
            plt.savefig(path+"thresholdCpGs_{:,.1f}.pdf".format(delta))
        plt.close()

def call_dmrs(classlevels, d, m, s):
    x = subprocess.Popen("diffmer dmrs {} --d {} --m {} --size {}".format(classlevels,d,m,s) , shell=True, stdout=subprocess.PIPE).stdout.read()
    if x is None or x == "":
        return 0
    x = x.decode("utf-8")

    for line in x.split("\n")[:-1]:
        if line.startswith("#"):
            continue
        chrom, start, stop, n, *_ = line.split("\t")
        yield n



def create_plot(figurename, xlabel, ylabel):
    plt.figure(figurename, figsize=(8, 6), dpi=80)
    plt.xlabel(xlabel) #Bezeichung x-Achse
    plt.ylabel(ylabel) #Bezeichnung y-Achse



def set_legend_position(ax, name =""):
    """Legend on the right"""
    # Shrink current axis by 20%
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
    # Put a legend to the right of the current axis
    fontP = FontProperties()
    fontP.set_size('small')
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), fancybox=True, prop = fontP, shadow=True,  title=name)



def plot_hist(classlevels, out_folder, eps):
    """plottet ein Histogramm der in data uebergebenen Daten
    range: (x, y)"""
    diff = []
    for chrom, data in classlevels:
        case = data["case"].values  # case methylation levels as numpy array
        control = data["control"].values  # dto for control
        diff_c = abs(case - control) # unsigned differences (numpy array)
        diff.append(diff_c)
    diffs = np.concatenate(diff)
    create_plot("Histogram", "Methylation difference", "Number of CpGs")
    bin_size = eps
    bin_number = int(round((1.0-eps)/bin_size,0))
    plt.hist(diffs, range = (eps,1.0),  bins = bin_number, color = "#84B818")
    plt.xlim(0, 1.0)

    if out_folder is None:
        plt.show()
    else:
        plt.savefig(out_folder+"hist.pdf")
    plt.close()



def main(args):
    if args.type == "histogram":
        data = io.read_classlevels(args.classlevels)
        eps = io.get_settings(args.classlevels)["precision"]
        plot_hist(data, args.output, eps)
    if args.type == "cpgs_per_threshold":
        plot_threshold_vs_CpGs(args.classlevels, args.output)
