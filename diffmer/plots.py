from . import io
from . import utilities
import os
import pandas as pd
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties
import matplotlib.patches as mpatches
from numba import jit
from collections import OrderedDict
import matplotlib.lines as mlines
import re
from bisect import bisect_left


def compplot(rawdata, case_list, control_list,  classlevels, dmrs, out_folder,  show_relative , show_bounds = True):
    """visualizes a given region by colouring the methylation get_level_values
    rawdata: dataframe with methylated / unmethylated CpGs
    case_list: List of names for case samples
    control_list: list of names for control samples
    classlevels: computed class methylation levels, optional
    dmrs: the regions to plot, given as chromosom, start, stop
    out_folder: where to save the result, optional
    show_relative: show real genom positions of relative positions starting at 1
    show_bound: shows 2 extra positions at both sides as context"""
    if out_folder is not None:
        if not os.path.exists(out_folder):
            os.makedirs(out_folder)
    chunk = None
    for chromosom_name, l, r in dmrs:
        reg = str(l)+"_"+str(r)#string fuer plot

        if (chunk is None) or (chromosom_name not in chunk.index.get_level_values(0)):
            for chunk in rawdata:                
                if chromosom_name in chunk.index.get_level_values(0):
                    break

        df = chunk.loc[chromosom_name]
        df.index = df.index.get_level_values(0)

        if show_bounds:
            pos = df.index.tolist()
            l_minus_1 = pos.index(l)-2
            r_plus_1 = pos.index(r)+3
            df = df.iloc[l_minus_1:r_plus_1]
        else:
            df = df.loc[l:r]

        if show_relative is True:
            #reset index
            new_index = [1]
            for x, y in utilities.pairwise(df.index):
                new_index.append(y-x)
            new_index = np.cumsum(new_index)
            df.index = new_index
            reg = str(1)+"_"+str(new_index[len(new_index)-1])#string fuer plot
        plot_comparative(df,(case_list,control_list), chromosom_name+"_" + reg, out_folder)

        if classlevels is not None:#also show computed results
            for chrom, cdata in classlevels:

                if chromosom_name == chrom:
                    break

            result_df = cdata

            if show_bounds:
                l_minus_1 = result_df.index.tolist().index(l)-2
                r_plus_1 = result_df.index.tolist().index(r)+3
                result_df = result_df.iloc[l_minus_1:r_plus_1]
            else:
                result_df = result_df.loc[l:r]
            plot_comparative(df,(case_list,control_list), chromosom_name+"_"+reg, out_folder, show_results = True, results = result_df )




def lineplot(rawdata, case_list, control_list,  classlevels, dmrs, out_folder,   prefs):
    """visualizes a given region by colouring the methylation level values,
    shows 2 extra positions at both sides as context
    rawdata: dataframe with methylated / unmethylated CpGs
    case_list: List of names for case samples
    control_list: list of names for control samples
    classlevels: computed class methylation levels, optional
    dmrs: the regions to plot, given as chromosom, start, stop
    out_folder: where to save the result, optional
    show_relative: show real genom positions of relative positions starting at 0 TODO"""
    context = 2
    import seaborn as sns
    sns.set_style("darkgrid", {'font.sans-serif': ['Helvetica', 'Liberation Sans', 'Bitstream Vera Sans', 'sans-serif']}) #change font to Helvetica to make negative Axis label possible (bug in seaborn?)
    if out_folder is not None:
        if not os.path.exists(out_folder):
            os.makedirs(out_folder)
    chunk = None
    classlevel_L = list(classlevels)
    for chromosom_name, l, r in dmrs:        
        identifier = (chromosom_name, l, r) 
        #find right chromosome
        if (chunk is None) or (chromosom_name not in chunk.index.get_level_values(0)):
            for chunk in rawdata:
                if chromosom_name in chunk.index.get_level_values(0):
                    break
        df = chunk.loc[chromosom_name]
        pd.set_option('mode.chained_assignment',None) #suppress warning, as there is nothing wrong here ( see http://pandas.pydata.org/pandas-docs/stable/indexing.html#indexing-view-versus-copy)
        df.sortlevel(axis=1,inplace=True, sort_remaining=True)
        df.index = df.index.get_level_values(0)
        result_df = chromosome_classlevel(iter(classlevel_L),chromosom_name)#classlevels.loc[chromosom_name]
        #show the two adjacent positions
        pos = df.index.tolist()

        l_pos, r_pos = interval_edges(pos, l,r)

        l_minus_1 = max(l_pos-context, 0)
        r_plus_1 = min(r_pos+context+1, df.shape[0])

        df = df.iloc[l_minus_1:r_plus_1]
        result_df = result_df.iloc[l_minus_1:r_plus_1]


        #if show_relative is True:#TODO
        #reset index
        new_index = [0]
        for x, y in utilities.pairwise(df.index):
            new_index.append(y-x)
        new_index = np.cumsum(new_index)
        #substract pos differences of context

        new_index = new_index - (df.index[context]-df.index[0])
        df.index = new_index
        result_df.index = new_index
        #compute methylation per sample by computing the local problem
        sample_meth = fill_sample_meth(df, result_df, prefs["aclass"])
        #compute variance out of sample meth
        std_dev = compute_standard_deviation(sample_meth,df)

        plot_lines(df, identifier, out_folder, result_df , sample_meth, std_dev, prefs["delta"], context)

def chromosome_classlevel(classlevels, chrom):
    for akt_chrom, data in classlevels:
        if chrom == akt_chrom:
            return data

def interval_edges(myList, left, right):
    """
    Assumes myList is sorted. Returns interval edges that include entries left and right
    """

    pos = bisect_left(myList, left)

    if (myList[pos] == left) or pos == 0:
        left_pos =  pos
    else:
        left_pos = pos -1

    #right edge

    pos = bisect_left(myList, right)

    if pos == len(myList):
        right_pos = pos-1
    else:
        right_pos = pos

    return left_pos, right_pos





def plot_lines(df, identifier, out_folder, results, sample_meth, std_dev, delta_thresh, context):
    (chromosom_name, l,r) = identifier
    name = "{}:{}-{}".format(chromosom_name, l,r+1)
    #prepare data
    idx = pd.IndexSlice
    C_case = df.loc[:,idx["case","M"]].values
    N_case = df.loc[:,idx["case","U+M"]].values
    C_control = df.loc[:,idx["control","M"]].values
    N_control = df.loc[:,idx["control","U+M"]].values


    fig = plt.figure()

    yheight = 0.8
    n,m = df.shape
    m = int(m/2)
    subtitles = ["{} samples, {} CpGs".format(m, n)]
    subtitle = "\n".join(subtitles)
    fig.suptitle(name, fontsize=14, x=0.54)
    cpgpos = list(df.index)

    ax = plt.subplot(111)
    ax.set_ylim([0,1])
    eps = 0.00000001
    for x in cpgpos:
        color = "black"#DataPoints
        if results.loc[x,"case"] < results.loc[x,"control"]:
            ymin = results.loc[x,"case"]
            ymax = results.loc[x,"control"]
            if results.loc[x,"control"] -  results.loc[x,"case"]+eps >= delta_thresh:
                color = "blue"
        else:
            ymax = results.loc[x,"case"]
            ymin = results.loc[x,"control"]
            if results.loc[x,"case"] - results.loc[x,"control"] +eps >= delta_thresh:
                color = "red"

        ax.axvline(x, ymin = ymin, ymax = ymax, color = color, alpha = 0.9)

    #Variance

    #for i, row in enumerate(sample_meth.values):#Zeile
        #for j, columns in enumerate(row):
            #plt.errorbar(x = cpgpos[i], y = columns, yerr=variance[i][j])
            #ax.axvline(cpgpos[i], ymin = columns - 0.02, ymax = columns+0.02, color = "red", alpha = 0.4, linewidth = 6)
    ax.plot(cpgpos, results.loc[:,"case"].values, "green")

    case_mask = np.in1d(list(sample_meth.columns.get_level_values(0)), "case")
    control_mask = np.in1d(list(sample_meth.columns.get_level_values(0)), "control")

    

    #for y in sample_meth["case"].values.transpose():
        #ax.plot(cpgpos, y, "green", alpha=0.1, linestyle="dotted")
    #DataPoints
    for i, y in enumerate((C_case/N_case).transpose()):

        #plt.errorbar(cpgpos, y, yerr=std_dev.transpose()[case_mask][i],alpha = 0.9, fmt = "_", markersize = 1.5, markeredgewidth = 0.5,color = "green", capthick=0.3, capsize = 1.2, linestyle="None", linewidth = 0.5)
        plt.errorbar(cpgpos, y, yerr=std_dev.transpose()[case_mask][i],alpha = 0.9,  marker = r"$\oplus$",  markersize = 4, color = "green", capthick=0.3, capsize = 1.2, linestyle="None", linewidth = 0.5)
        #plt.errorbar(cpgpos, y, yerr=[0.2 for _ in cpgpos], fmt="-o", capthick=2, capsize = 2)
        #ax.plot(cpgpos, y, "green", alpha=0.9, marker = r'$\oplus$', markersize=5, linestyle = "None")#, linestyle="--")

    ax.plot(cpgpos, results.loc[:,"control"].values, "purple")#, label="class methylation control")
    #for y in sample_meth["control"].values.transpose():
        #ax.plot(cpgpos, y, "purple", alpha=0.1, linestyle="dotted")#, label= control_list[i]+" (computed)")
    for i, y in enumerate((C_control/N_control).transpose()):
        plt.errorbar(cpgpos, y, yerr=std_dev.transpose()[control_mask][i], marker = r"$\ominus$", markersize=4, color = "purple", capthick=0.3, capsize = 1.2, linestyle="None", linewidth = 0.5)
        #plt.errorbar(cpgpos, y, yerr=std_dev.transpose()[control_mask][i], color = "purple",fmt = "_", markersize = 1.5, markeredgewidth = 0.5, capthick=0.3, capsize = 1.2, linestyle="None", linewidth = 0.5)
        #ax.plot(cpgpos, y, "purple", alpha=0.9,  marker = r'$\ominus$', markersize=5, linestyle="None")



    #not a label for every line, but a common label for all types
    patches = []
    patches.append(mpatches.Patch(color='blue', alpha = 0.9, label='hypomethylation'))
    patches.append(mpatches.Patch(color='red', alpha = 0.9, label='hypermethylation'))
    patches.append(mpatches.Patch(color='black', alpha = 0.9, label='difference $< \delta$'))

    patches.append( mpatches.Patch( color='green', label='class meth. case (computed)'))
    patches.append( mpatches.Patch( color='purple', label='class meth. control (computed)'))
    patches.append( mlines.Line2D([], [], color='green',alpha=0.9, marker=r'$\oplus$', markersize=8, linestyle = "None",label='sample meth. case (measured)'))
    #patches.append(mpatches.Patch(color='green',alpha=0.1,  label='sample meth. case (computed)'))
    patches.append( mlines.Line2D([], [], color='purple',alpha=0.9, marker=r'$\ominus$', markersize=8, linestyle = "None",label='sample meth. control (measured)'))
    #patches.append( mlines.Line2D([], [], color='purple',alpha=0.9, marker="_", markersize=5,markeredgewidth = 1.0,  linestyle = "None",label='sample meth. control (measured)'))
    #patches.append(mpatches.Patch(color='purple',alpha=0.1,  label='sample meth. control (computed)'))

    plt.xlabel("relative position")
    plt.ylabel("methylation level")
    set_legend_position_below(ax, patches, 3)
    
    
    #grey region
    ax.axvspan(xmin=cpgpos[0], xmax=cpgpos[context], color="grey", alpha = 0.3)
    ax.axvspan(xmin=cpgpos[-context-1], xmax=cpgpos[-1], color="grey", alpha = 0.3)
    ax.set_xlim([cpgpos[0], cpgpos[-1]])
    #n = len(ax.xaxis.get_ticklabels())           # count labels
    #ax.set_xticklabels(np.linspace(cpgpos[0], cpgpos[-1], n))
    if out_folder is None:
        plt.show()
    else:
        number = re.findall('\d+', chromosom_name)
        if number:
            number = int(number[0])

            fname = "chr{:02d}__{:09d}_{:09d}".format(number, l,r)
        else:
            fname = "{}__{:09d}_{:09d}".format(chromosom_name, l,r)
        fig.savefig(out_folder+fname+".pdf")

    plt.close(fig)

    fig = None        


def compute_standard_deviation(sample_meth, df):
    """computes standard deviation of each data point
    sample_meth: numpy array of sample methylation values
    df: DataFrame of Counts (Cs, Ns)"""
    idx = pd.IndexSlice
    N = df.loc[:,idx[:,"U+M"]].values
    mu = sample_meth.values
    variance = np.array(mu*(1-mu), dtype = float)/ N
    error = np.sqrt(variance)

    return error


def fill_sample_meth(df,result_df, aclass):
    """computes the sample methylation levels for every sample/ CpG site
    result_df: class methylation values for every class / CpG site
    df: dataframe of Counts (Cs, Ns)"""
    idx = pd.MultiIndex.from_tuples([("case", s) for s in
              list(OrderedDict.fromkeys(df["case"].columns.get_level_values(1).values)) ] 
              + [("control", s) for s in list(OrderedDict.fromkeys(df["control"].columns.get_level_values(1).values)) ])

    sample_meth = pd.DataFrame(index = df.index, columns = idx)
    for clas in ["case", "control"]:
        frame = df[clas]
        class_meth = result_df.loc[:,clas]
        _fill_sample_meth(class_meth, frame, sample_meth, aclass)
    return sample_meth


def _fill_sample_meth(class_meth, frame, sample_meth, aclass):
    idx = pd.IndexSlice
    C_class = frame.loc[:, "M"]
    N_class = frame.loc[:, "U+M"]
    for ind in class_meth.index:
        for name in list(OrderedDict.fromkeys(frame.columns.get_level_values(1).values)): #preserve order!
            N = N_class.loc[ind, name]
            if N == 0:
                sample_meth.loc[ind, idx[:, name]] = 0
                break
            C = C_class.loc[ind, name]
            w = (0.0 + N*(N+2)**2) / (2*(C+1)*(N-C+1))
            p = C/N
            q = class_meth.loc[ind]
            #sample_meth.loc[ind, name]
            sample_meth.loc[ind, idx[:, name]] = minimize_weighted_absolute_and_squared_distance(w,p,aclass,q)



@jit(nopython=True)
def minimize_weighted_absolute_and_squared_distance(w, p, v, q):
    """
    min w|x-p|²+v(x-q)
    returns x
    """
    if q == p:
        return p
    if q < p:
        x =  max(p-v/(2*w), q)
    else:
        x = min(p+v/(2*w), q)
    return x



def set_legend_position(ax, name =""):
    """Legende rechts neben Grafik"""
    # Shrink current axis by 20%
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

    # Put a legend to the right of the current axis
    fontP = FontProperties()
    fontP.set_size('small')
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.25), fancybox=True, shadow=True, prop = fontP, title=name)


def set_legend_position_below(ax, handles, ncols = 2):
    """setzt die Legende von ax unter die grafik"""
    #Aussehen konfigurieren
    box = ax.get_position()
    ax.set_position([box.x0, box.y0 +box.height*0.2, box.width, box.height * 0.9])
    fontP = FontProperties()
    fontP.set_size('small')
    # Put a legend below current axis
    ax.legend(loc='upper center', bbox_to_anchor=(0.42, -0.1), fancybox=True, shadow=True, ncol=ncols, prop = fontP, handles = handles)

def set_alpha(N, i, j):
    val = N[i, j]
    if val > 20:
        val = 20
    alpha = val /20
    return alpha

def mycmap(x, N):
    #customized colormap. Alphachannel depends on Coverage
    colors = ["#4444dd", "#dd4444"]  # (blue -> red)
    fontcolor = lambda x: "#ffffff"
    mycolormap = mpl.colors.LinearSegmentedColormap.from_list("mycolormap", colors)

    tmp = mycolormap(x)
    #print(tmp)
    for i in range(tmp.shape[0]):
        for j in range(tmp.shape[1]):
            tmp[i,j][3] = set_alpha(N, i, j)
            #tmp[i,j][3] = 1.0
    return tmp

#########################################################
# comparative methylation plot

def plot_comparative(df,  samplenames, fname, out_folder, format="pdf", show_results = False, results = None):
    """
    Create and show/save a comparative methylation plot.
    df: DataFrame with the region to plot. Contains counts and a single index column
    samplenames: (case sample names, control sample names)
    fname: filename of the resulting image file
    out_folder: folder to write plot
    format: image format (e.g., 'png', 'pdf', 'svg')
    """
    #prepare data
    case_list = samplenames[0]
    control_list = samplenames[1]

    case = df["case"]
    C_case = case.loc[:, "M"].values.transpose()
    N_case = case.loc[:, "U+M"].values.transpose()

    control = df["control"]
    C_control = control.loc[:, "M"].values.transpose()
    N_control = control.loc[:, "U+M"].values.transpose()

    C = np.vstack((C_case, C_control))
    N = np.vstack((N_case, N_control))
    analysis = C / N

    case_rates = C_case/N_case
    control_rates = C_control/N_control
    mean_case = np.sum(C_case, axis=0)/np.sum(N_case, axis=0)
    mean_control = np.sum(C_control, axis = 0)/np.sum(N_control, axis=0)
    if show_results == True:
        assert results is not None
        result = np.array(results.as_matrix()).transpose()

        array = np.vstack((case_rates, mean_case, result, mean_control, control_rates ))
        coverage = np.vstack((N_case, np.sum(N_case, axis=0), np.sum(N_case, axis=0), np.sum(N_control, axis=0), np.sum(N_control, axis=0), N_control ))
    else:

        array = np.vstack((case_rates, mean_case, mean_control, control_rates))
        coverage = np.vstack((N_case, np.sum(N_case, axis=0), np.sum(N_control, axis=0),  N_control ))

    # determine cpg positions
    cpgpos = df.index
    if cpgpos is None:  return False  # inconsistent CpGs
    
    n,m = df.shape
    m = int(m/2)
    assert n is not None

    # initialize figure, set figure title/remark and axis title (subtitle)
    fig = plt.figure()

    titles = ["Comparison: " + fname]
    yheight = 0.8
    subtitles = ["{} samples, {} CpGs".format(m, n)]
    title = "\n".join(titles)
    subtitle = "\n".join(subtitles)
    fig.suptitle(title, fontsize=14, x=0.54)
    # if there is not enough space for labels at the left side,
    # increase the 'left' coordinate and reduce the 'width' in the following line
    ax = fig.add_axes([0.12, 0.1, 0.85, yheight]) # left, bottom, width, height    
    ax.set_title(subtitle, fontsize=12)

    # plot image
    image = ax.imshow(mycmap(array,coverage), 
        interpolation='none', origin='upper', vmin=0.0, vmax=1.0)
    ax.axhline(y=len(case_list)-0.5,  color='black')
    ax.axhline(y=len(case_list)+1.5,  color='black')
    if show_results == True:
        ax.axhline(y=len(case_list)+3.5,  color='black')

    ax.set_aspect('auto')
    xfontsize = 8 if n < 20 else 6
    #fontcolor = lambda x: "#ffffff" if x>0.5 else "#000000"
    fontcolor = lambda x: "#ffffff"
    #only show values for small dmrs
    if show_results == True and n<= 20:
        for i in range(m+4):
            for j in range(n):
                x = array[i,j]
                ax.text(j,i, "{:,.0%}".format(x), fontsize=xfontsize, color=fontcolor(x), ha='center')#Methylierung in %
                ax.text(j,i+0.2, "{:d}".format(coverage[i,j]), fontsize=xfontsize, color=fontcolor(x), ha='center')#Coverage
    else:
        if n<= 20:
            for i in range(m+2):
                for j in range(n):
                    x = array[i,j]
                    ax.text(j,i, "{:,.0%}".format(x), fontsize=xfontsize, color=fontcolor(x), ha='center')#Methylierung in %
                    ax.text(j,i+0.2, "{:d}".format(coverage[i,j]), fontsize=xfontsize, color=fontcolor(x), ha='center')#Coverage

    # column-wise methylation rates
    avgcolrates = np.mean(analysis, axis=0)
    x1labels = ["{:,.2f}".format(100*m) for m in avgcolrates]  
    x2labels = label_formatter(cpgpos)
    xlabels = [x1+"\n"+x2 for x1,x2 in zip(x1labels,x2labels)]    
    ax.set_xlabel('Average methylation level [%] / CpG-site')
    ax.set_xticks(range(n))
    ax.set_xticklabels(xlabels, fontsize=xfontsize)
    y1labels = case_list + ["mean_case", "mean_control"] + control_list
    y2labels = [ "(data)" for _ in range(len(y1labels))]
    if show_results == True:
        y1labels = y1labels[0:len(case_list)+1] + [r"$\mu_{i, \oplus}$" , r"$\mu_{i, \ominus}$" ] + y1labels[len(case_list)+1 : len(y1labels)]
        y2labels = y2labels[0:len(case_list)+1] + ["(computed)", "(computed)"] + y2labels[len(case_list)+1 : len(y1labels)]


    ylabels = [y1+"\n"+y2 for y1,y2 in zip(y1labels,y2labels)]
    ax.set_yticks(range(m+4)) if show_results else ax.set_yticks(range(m+2))
    yfontsize = 8 if m < 21 else 6
    ax.set_yticklabels(ylabels, fontsize=yfontsize)

    # save to file
    if out_folder is None:
        plt.show()
    else:
        if show_results == True:
            fig.savefig(out_folder+fname+"comp.pdf", format=format)  # bbox_inches="tight" cuts off title!
        else:
            fig.savefig(out_folder+fname+".pdf", format=format)  # bbox_inches="tight" cuts off title!
    plt.close(fig)
    fig = None



def label_formatter(positions, digits=3):
    """gets a list of positions and formats them"""
    labels = [".."+str(pos)[-digits:] if len(str(pos)) > 3 else str(pos)[-digits:] for pos in positions]
    return labels




def main(args):
    (samplenames, cases, controls) = io.get_samplenames(args.classes, args.classfile)
    data = io.read_input_file(args.input, samplenames, cases, args.index, args.format)
    if args.classlevels is not None:
        classlevels = io.read_classlevels(args.classlevels)
    else:
        classlevels = None
    if args.type == "compplot":
        compplot(data, cases, controls, classlevels, io.read_dmrs(args.dmrs, args.manual_region), args.output, args.relative_positions, show_bounds=True)
    else:
            #infer alpha_class
        if args.classlevels is None:
            raise RuntimeError("no classlevels given (but needed for lineplot); use --classlevels")
        prefs = {}
        if args.dmrs is not None:
            prefs = io.get_settings(args.dmrs)
        else:
            prefs["aclass"] = args.aclass

        if "aclass" not in prefs.keys():
            prefs["aclass"] = 17.82
        #check for user-specified value and rewrite it
        if args.delta is not None:
            prefs["delta"] = args.delta

        if not "delta" in prefs:
            raise RuntimeError("no threshold delta was given for lineplot); use --delta to provide one")
        lineplot(data, cases, controls, classlevels, io.read_dmrs(args.dmrs, args.manual_region), args.output, prefs)

