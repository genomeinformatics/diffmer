# diffmer.estimate module

import sys
import os
import time
from itertools import chain
from functools import partial
from concurrent.futures import ThreadPoolExecutor
from multiprocessing import cpu_count
import numpy as np
import pandas as pd
from numba import jit
from . import io
from . import pValues
#from collections import OrderedDict
import h5py
sys.path.append(os.path.abspath("/scratch/flasiax/flasiax/"))
#from iterative import *


@jit(nopython=True)
def minimize_weighted_absolute_and_squared_distance( v, q, w, p):
    """
    Computes the minimum of w(x-p)²+v|x-q| with w, v > 0
    """
    if q == p:
        return 0
    if q < p:
        x =  max(p-v/(2*w), q)
    else:
        x = min(p+v/(2*w), q)
    f_x = w*(x-p)**2+v*abs(x-q)
    return f_x
    


@jit(nopython=True)
def local_costs(localcosts, aclass, M, Cs, Ns):
    """computes local = vertical costs for a single class and position
    the number of possible costs is determined by M
    localcosts: empty array of same size of M 
    aclass: weight
    M: array of possible states
    Cs, Ns: Counts for all samples"""
    states = M.shape[0]
    samples = Ns.shape[0]
    localcosts[:] = 0
    for t in range(samples):
        N = Ns[t]
        C = Cs[t]
        ## assert N >= C
        if N > 0:  # keine Kosten fuer 0/0
            w2 = (0.0 + N*(N+2)**2) / (2*(C+1)*(N-C+1))
            for idx in range(states):
                m = M[idx]
                localcosts[idx] += minimize_weighted_absolute_and_squared_distance(aclass, m, w2, C/N)
    return localcosts


@jit(nopython=True)
def add_local_costs(this_V, M, adiff, aclass, Cs_case, Ns_case, Cs_control, Ns_control,
                    localcosts_case, localcosts_control):
    """add local costs to this_V"""
    states = M.shape[0]
    local_costs(localcosts_case, aclass, M , Cs_case, Ns_case)
    local_costs(localcosts_control, aclass, M , Cs_control, Ns_control)
    for ix in range(states):
        for iy in range(states):
            this_V[ix,iy] += localcosts_case[ix] + localcosts_control[iy] + adiff * abs(M[ix] - M[iy])


predecessor_dtype = np.dtype([('x', np.uint8), ('y', np.uint8), ('cost', np.float64)])
@jit(nopython=True)
def get_predecessors(last_V, aspace, predecessors):
    states = last_V.shape[0]
    # filter out entries which are greater than upper_bound_best_V
    upper_bound_best_V = last_V.min() + aspace * 2.0
    count = 0
    for jx in range(states):
        for jy in range(states):
            last_Vj = last_V[jx,jy]
            if last_Vj <= upper_bound_best_V:
                predecessors[count].x = jx
                predecessors[count].y = jy
                predecessors[count].cost = last_Vj
                count += 1
    return predecessors[:count]


@jit(nopython=True)
def add_space_costs(this_V, this_T, aspace, dspace, predecessors):
    """
    advance to next position, add space costs to this_V, and log traceback in this_T.
    """
    states = dspace.shape[0]
    predecessors = get_predecessors(this_V, aspace, predecessors)

    for ix in range(states):
        for iy in range(states):
            (best_jx, best_jy, best_V) = (0, 0, np.float64(np.inf))
            for pre in predecessors:
                (jx, jy) = (pre.x, pre.y)
                from_pre = pre.cost + aspace * (dspace[iy,jy] + dspace[ix,jx])
                if from_pre < best_V:
                    (best_jx, best_jy, best_V) = (jx, jy, from_pre)
            this_V[ix,iy] = best_V
            this_T[ix,iy,0] = best_jx
            this_T[ix,iy,1] = best_jy


@jit(nopython=True)
def do_traceback(classlevels, M, T, ix, iy):
    """
    compute traceback on T, starting at (-1,x,y).
    M contains the discrete methylation levels.
    classlevels is the Nx2 array to be written to
    """
    N = T.shape[0]+1
    classlevels[-1,0] = M[ix]
    classlevels[-1,1] = M[iy]
    for i in range(N-2, -1, -1):
        (ix, iy) = (T[i,ix,iy,0], T[i,ix,iy,1])
        classlevels[i,0] = M[ix]
        classlevels[i,1] = M[iy]


@jit(nopython=True, nogil=True)
def process_subproblem_(classlevels, a_class, a_diff, M, d_space,
                        a_space_table, C_case, N_case, C_control, N_control,
                        this_V, localcosts_case, localcosts_control, predecessors, traceback):
    states = M.shape[0]
    N = C_case.shape[0]

    add_local_costs(this_V, M, a_diff, a_class, C_case[0], N_case[0], C_control[0], N_control[0],
                    localcosts_case, localcosts_control)

    for n in range(1, N):
        add_space_costs(this_V, traceback[n-1], a_space_table[n-1], d_space, predecessors)
        add_local_costs(this_V, M, a_diff, a_class, C_case[n], N_case[n], C_control[n], N_control[n],
                        localcosts_case, localcosts_control)
    imin = this_V.argmin()
    ix = imin // states
    iy = imin %  states

    do_traceback(classlevels, M, traceback, ix, iy)


def process_subproblem(a_class, a_diff, M, d_space, subproblem):
    (chrom, pos, a_space_table, C_case, N_case, C_control, N_control) = subproblem


    states = M.shape[0]
    N = C_case.shape[0]

    this_V = np.zeros((states, states), dtype=np.float64)
    localcosts_case = np.empty((states,), dtype=np.float64)
    localcosts_control = np.empty((states,), dtype=np.float64)
    predecessors = np.empty(states * states, dtype=predecessor_dtype)
    traceback = np.empty((N-1, states, states, 2), dtype=np.uint8)
    classlevels = np.empty((N,2), dtype=np.float64)

    #call_flsa(C_case, N_case, C_control, N_control, a_class, a_diff)
    process_subproblem_(classlevels, a_class, a_diff, M, d_space,
                        a_space_table, C_case, N_case, C_control, N_control,
                        this_V, localcosts_case, localcosts_control, predecessors, traceback)
    return (chrom, pos, classlevels)



def precompute_a_space(positions, m, b, s_space):
    '''
    Given a numpy array 'positions',
    Return array a_space such that
    a_space[i] is the a_space weight between CpGs i and i+1
    '''
    distances = np.diff(positions)
    a_space = s_space * np.log(2) / (m*distances + b)
    return a_space


def separate_subproblems(chrom, data, a_space_weights):
    """
    yield for each subproblem (chrom, pos, a_space_table, C_case, N_case, C_control, N_control).
    The problem is separated where a_space drops below cut_weight, subproblems can be solved parallely
    a_space_weights: (a_space_slope, a_space_offset, a_space_scale, cut_weight)
    case_list, control_list: Lists of sample names
    eps: precision
    cut_weight: threshold for a_space
    """
    # get individual weights
    (slope, offset, scale, cut_weight) = a_space_weights
    #pos = data.index.get_level_values('pos').values  # numpy array
    pos = data.index.get_level_values(0).values  # numpy array
    a_space_table = precompute_a_space(pos, slope, offset, scale)
    case = data["case"]
    #case.sortlevel(axis=0,inplace=True,sort_remaining=True)
    #pd.set_option('mode.chained_assignment',None) #suppress warning, as there is nothing wrong here ( see http://pandas.pydata.org/pandas-docs/stable/indexing.html#indexing-view-versus-copy)
    #case = case.sortlevel(axis=1, sort_remaining=True)
    C_case = case.loc[:,"M"].values
    N_case = case.loc[:, "U+M"].values
    del case
    control = data["control"]
    C_control = control.loc[:, "M"].values
    N_control = control.loc[:, "U+M"].values
    del control

    N = data.shape[0]  # number of CpGs
    segment_end_indices = np.where(a_space_table < cut_weight)[0] + 1
    segments = zip(chain((0,), segment_end_indices), chain(segment_end_indices, (N,)))
    for i, j in segments:
        yield (chrom, pos[i:j], a_space_table[i:j-1], C_case[i:j], N_case[i:j], C_control[i:j], N_control[i:j])



def get_subproblems(data, aspace_weights):
    #chromosomes = list(OrderedDict.fromkeys(data.index.get_level_values('chr'))) #preserve order!
    for chrom_data in data:
        chrom = chrom_data.index.get_level_values(0)[0]

        # note: if chromosome awareness would be added to segmentation in separate_subproblems,
        # there wouldn't be any need to process by chromosome anymore.
        # process cpgs in a single chromosome
        cdata = chrom_data.loc[chrom] #different format
        yield from separate_subproblems(chrom, cdata, aspace_weights)




def process_data(data, weights, eps, timer, f):
    aclass, adiff, *aspace_weights = weights

    states = 1+int(round(1/eps))  # number of discrete methylation levels
    M = np.round(np.linspace(0.0, 1.0, states, dtype=np.float64), 8)  # set of discrete methylation levels
    assert M.shape[0] == states

    d_space = np.abs(M[:,np.newaxis] - M)
    _process_subproblem = partial(process_subproblem, aclass, adiff, M, d_space)

    with ThreadPoolExecutor(cpu_count()) as executor:
        subproblems = get_subproblems(data, aspace_weights)
        akt_chrom = None

        for chrom, pos, classlevels in executor.map(_process_subproblem, subproblems):
            # set up and write DataFrame
            #idx = pd.MultiIndex.from_product([[chrom], pos], names=["chr", "pos"])
            #result = pd.DataFrame(classlevels, index=idx, columns=["case", "control"],)
            #result.to_csv(sys.stdout, header=False, sep="\t")
            if akt_chrom == chrom:
                #enlarge data
                _classlevels = np.vstack([_classlevels, classlevels])
                _pos = np.concatenate([_pos, pos])
            elif akt_chrom is None:
                akt_chrom = chrom
                _classlevels = classlevels
                _pos = pos
            else:
                f.create_dataset("classlevels/"+akt_chrom, data = _classlevels)
                f.create_dataset("positions/"+akt_chrom, data = _pos)
                _classlevels = classlevels
                _pos= pos
                akt_chrom = chrom

            #sys.stdout.flush()
            if timer is not None:
                timer.print("   {}: finished subproblem of size: {}".format(chrom, pos.shape[0]))
        f.create_dataset("classlevels/"+akt_chrom, data = _classlevels)
        f.create_dataset("positions/"+akt_chrom, data = _pos)



def compute_pValues(data, weights, eps, f, N = 1):
    """shuffles data N times, computes classlevels and scores them"""
    luts = None
    for _ in range(N):
        new_data = pValues.redistribute_data(data) 
        #compute classlevels
        store = h5py.File("simulated.h5", "w") 
        process_data(new_data, weights,eps , None, store)
        store.close()
        del new_data
        classlevels = io.read_hdf5classlevels("simulated.h5")
        lut = pValues.compute_score_hist(classlevels, eps)
        del classlevels
        if luts is None:
                luts = lut
        else:
            #add up all counts
            for i, subtable in enumerate(lut):
                luts[i] += subtable

    param_list = pValues.extrapolate_and_convert_to_pValues(luts, eps, func= pValues.linear_func)
    f.create_dataset("pValues", data = param_list)
    os.remove("simulated.h5")

    #data = io.read_whole_dmrs(args.dmrs)
    



class Timer:
    """callabe timer that returns minutes since start"""
    def __init__(self, start=time.time()):
        self.start = start
    def __call__(self):
        return (time.time() - self.start) / 60.0
    def print(self, msg, file=sys.stderr):
        print("{:.3f}: {}".format(self(), msg), file=file)


def ToJulia(data, weights, filename):
    ##call ist with
    #output to Elias's Julia Implementation
    for chrom_data in data:
        chrom = chrom_data.index.get_level_values(0)[0]
        cdata = chrom_data.loc[chrom] #different format
        _ToJulia(cdata, weights, filename, chrom)

def _ToJulia(data, weights, filename, chrom):
    #####
    import os
    print("_ToJulia %s in %s" % (filename, os.getcwd()), file=sys.stderr)
    aclass, adiff, *aspace_weights = weights
    (slope, offset, scale, cut_weight) = aspace_weights
    with  h5py.File("{}.{}.flsa.h5".format(filename, chrom), "w") as f:
        nodes = f.create_group("nodes")
        edges = f.create_group("edges")
        #-----------
        N = data.shape[0]  # number of CpGs = steps in the ladder graph

        #-----------------------
        #add nodes
        #add normal nodes

        nodes_input  = [0 for _ in range(2*N)]
        nodes_weight = [0 for _ in range(2*N)]

        #add data
        case = data["case"]
        C_case = case.loc[:,   "M"].values
        N_case = case.loc[:, "U+M"].values
        del case
        control = data["control"]
        C_control = control.loc[:,   "M"].values
        N_control = control.loc[:, "U+M"].values
        del control
        samples_case    = C_case.shape[1]
        samples_control = C_control.shape[1]

        qweights_case    = np.array(0.0 + N_case   *(N_case+2)**2)    / (2*(C_case+1)   *(N_case   -C_case+1))
        qweights_control = np.array(0.0 + N_control*(N_control+2)**2) / (2*(C_control+1)*(N_control-C_control+1))
        nodes_weight = np.concatenate((nodes_weight, qweights_case.flatten(), qweights_control.flatten()))

        nodes.create_dataset("weight", data=nodes_weight, dtype="f")
        nodes_input = np.concatenate((nodes_input, (C_case/N_case).flatten(), (C_control/N_control).flatten() ))
        nodes.create_dataset("input", data=nodes_input, dtype="f")

        #-----------------------
        #add edges
        #add aspace
        pos = data.index.get_level_values(0).values  # numpy array
        a_space_table = precompute_a_space(pos, slope, offset, scale)
        head_space = np.append(np.arange(1, N) , np.arange(N+1, 2*N))
        tail_space = np.append(np.arange(2,N+1), np.arange(N+2, 2*N+1))
        weights_space = np.append(a_space_table, a_space_table)

        methyl = f.create_group("methyl")
        methyl.create_dataset("C_case", data=C_case, dtype="i")
        methyl.create_dataset("N_case", data=N_case, dtype="i")
        methyl.create_dataset("C_control", data=C_control, dtype="i")
        methyl.create_dataset("N_control", data=N_control, dtype="i")
        methyl.create_dataset("adiff",  data=adiff,         dtype="d")
        methyl.create_dataset("aspace", data=a_space_table, dtype="d")
        methyl.create_dataset("aclass", data=aclass,        dtype="d")

        #add adiff

        head_diff = np.arange(1, N+1)
        tail_diff = head_diff+N
        weight_diff = [adiff for _ in range(N)]

        #add aclass

        heads_aclass = []
        tails_aclass = []
        for i in range(1, samples_case+1):
            heads_aclass.extend(np.arange(1, N+1))
            tails_aclass.extend(np.arange(2*N+i, 2*N+samples_case*N+1, samples_case))

        for i in range(1, samples_control+1):
            heads_aclass.extend(np.arange(N+1, 2*N+1))
            tails_aclass.extend(np.arange(2*samples_case*N+i, 2*samples_case*N+samples_control*N+1, samples_control))
        weight_aclass = [aclass for _ in range(N*(samples_case+samples_control))]
        #add all edges
        head = np.concatenate((head_space,head_diff,heads_aclass))

        tail = np.concatenate((tail_space,tail_diff,tails_aclass))
        weight = np.concatenate((weights_space,weight_diff,weight_aclass))

        edges.create_dataset("head", data=head, dtype="i")
        edges.create_dataset("tail", data=tail, dtype="i")
        edges.create_dataset("weight", data=weight, dtype="f")






def call_flsa(C_case, N_case, C_control, N_control,a_class, a_diff):
    C = np.hstack((C_case, C_control))
    N = np.hstack((N_case, N_control))
    S = np.nan_to_num(C/N)
    classes = [1 for _ in range(C_case.shape[1])] + [0 for _ in range(C_control.shape[1])]
    samples = C_case.shape[1] + C_control.shape[1]
    #quadratic weights
    qweights = (0.0 + N*(N+2)**2) / (2*(C+1)*(N-C+1))



    twoclass_multisample_flsa(S, classes, quadweights=qweights, absweights=a_diff, scweights=a_class, prec=1/32, out_class=None, out_sample=None,
        initialrange=None, extendrange=False, extenddirection=-1, levels=5)



def main(args):

    timer = Timer() if args.times else None
    if timer is not None:
        timer.print("read data")

    (samplenames, cases, controls) = io.get_samplenames(args.classes, args.classfile)

    aspace_weights = (args.slope, args.offset, args.scale, args.cut)
    weights = (args.aclass, args.adiff) + aspace_weights

    data = io.read_input_file(args.input, samplenames, cases, args.index, args.format)

    L = list(data)
    # weights = [a_class, a_diff, slope_a_space_slope, offset_a_space, scale_a_space]
    # default: weights = [17.82, 11.09, 0.00224, 0.033, 3.0]
    # other default: weights= [34.83 , 16.75, 0.0018, 0.054, 3.0]
    # for 1 vs 1:  weights = [17.82, 0.5*11.09, 0.00224, 0.033, 2.0]
    #f = io.create_hdf5file(args.output)
    f = io.write_settings(args)
    #store = pd.HDFStore(args.output)

    if args.flsa is not None:
        timer.print("ToJulia %s" % args.flsa)
        ToJulia(L, weights, args.flsa)

    if timer is not None:
        timer.print("process data")
    #print("#aclass={}\n#adiff={}\n#aspace slope={}\n#aspace offset={}\n#aspace scale={}\n#cut={}\n#precision={}".format(args.aclass, args.adiff, args.slope, args.offset, args.scale, args.cut, args.precision))
    #if args.pValues:
        #compute_pValues(iter(L),weights, args.precision, f, args.resamples)
    process_data(iter(L), weights, args.precision, timer, f)
    f.close()
    if timer is not None:
        timer.print("done")

