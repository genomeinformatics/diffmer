__version__ = "0.9"

DESCRIPTION = """
diffmer (Differentially Methlyated Regions)
is a tool that uses LASSO-inspired optimization 
to estimate methylation levels of CpG sites in samples
and in classes of samples.
It then detects differentially methylated CpGs (DMCs)
and differentially methylated regions (DMRs).
"""


