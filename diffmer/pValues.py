
import sys
import pandas as pd
import numpy as np
from scipy import stats
from . import io
#from . import estimate
#from . import newcaller as nc
import matplotlib.pyplot as plt
import random
from collections import OrderedDict
#from contextlib import redirect_stdout
#import math
from scipy.optimize import curve_fit


def redistribute_data(data):
    """redistributes data chromosomewise by changing the order of CpG positions. The data at every position is left unchanged"""
    for chunk in data:
        ix = np.array(chunk.index.get_level_values(1)) #positions
        random.shuffle(ix)
        arrays = [chunk.index.get_level_values(0), ix, ix+1]
        new_index = pd.MultiIndex.from_tuples(list(zip(*arrays)), names = ["chr", "start", "stop"])

        #create multiindex
        new_df = pd.DataFrame(index = new_index, data = chunk.values, columns = chunk.columns)

        new_df.sort_index(inplace=True) #level = 1, 
        yield new_df

def compute_score_hist(classlevels, eps):
    """computes a histogram of the scores of all length l intervals in the given classlevels"""
    hist_lut = []
    for l in range(1,30):
        scores = scores_for_length( classlevels,l)
        #compute histogram
        bin_number = (l/eps)+1
        distr = np.histogram(scores, bin_number, range = (0,1+eps))[0]
        hist_lut.append(distr)
    return np.array(hist_lut)



def scores_for_length(classlevels, l):
    """computes scores (average of methylation differences) for every chunk of size l"""

    
    #chromosomes = list(OrderedDict.fromkeys(classlevels.index.get_level_values('chr'))) #preserve order!
    #compute scores for various lengths
    scores = []
    for chrom, cdata in classlevels:
        #cdata = classlevels.loc[chrom]
        n = cdata.shape[0]
        case = cdata["case"].values  # case methylation levels as numpy array
        control = cdata["control"].values  # dto for control
        diff = case - control  # signed differences (numpy array)
        averages = moving_average(np.abs(diff), l)
        scores.extend(averages)
        #all scores for given length

    print(scores)
    return scores

def moving_average(x, windowsize, divide=True):
    """
    Return moving averages of x with a given windowsize.
    The length of the result is len(x)-windowsize+1.
    
    If not a moving average, but a moving sum is desired, use divide=False.
    """
    n = windowsize
    ret = np.cumsum(x, dtype=np.float64)
    ret[n:] = ret[n:] - ret[:-n]
    if divide:
        return ret[n-1:] / n
    return ret[n-1:]



def compute_pvalue( lut, eps, dmr):
    """computes p-Values (as -10 log_10(pValue)) for given DMRs based on computed scores for random data
    !!!currently pValue!!
    lut: look up table with cumulated number of scores for different lengths
    eps: precision, is needed to compute bin size/ number"""


    #compute score
    l = len(dmr)
    score = abs(sum(float(x) for x in dmr))/l
    #look up score in lut
    if l > len(lut): #if no values are available, take largest 
        l = len(lut)

    mL = lut[l-1][0]
    bL = lut[l-1][1]
    pValue = mL * score + bL
    #return -10*pValue
    return 10**pValue





def create_plot(figurename, xlabel, ylabel):
    plt.figure(figurename, figsize=(8, 6), dpi=80)
    plt.xlabel(xlabel) #Bezeichung x-Achse
    plt.ylabel(ylabel) #Bezeichnung y-Achse


def plot_scores(eps, l, normed_logdata, extrapolation):
    import seaborn as sns
    sns.set_style("darkgrid", {'font.sans-serif': ['Helvetica', 'Liberation Sans', 'Bitstream Vera Sans', 'sans-serif']})

    create_plot("Histogram", "Score", "log(p)")
    ax = plt.subplot(111)
    x = np.linspace(0,1, len(normed_logdata))
    ax.plot(x, extrapolation,  color = "black")
    bin_size = eps/l

    ax.bar(x, normed_logdata, bin_size, color = "#84B818")
    # put the major ticks at the middle of each cell
    #ax.set_xticks(x+eps, minor=False) 
    plt.xlim((0,1+eps))
    plt.savefig("scores for length {}.pdf".format(l))
    plt.close()



def linear_func(x, a, b):
    return a*x + b


def log_func(x, a, b):
    return np.float64(10**(a*x+b))


def extrapolate_and_convert_to_pValues(luts, eps, func, plot=False):
    """computes cumulated sum from the rights,
    extrapolates missing values by fitting a function func to all
    non zero entries, and turns luts into an array of pValues
    luts: lookup tables with number of scores for different interval lengths
    eps: precision, used to infer bin number
    func: function to be fitted"""
    param_list = []
    for l in range(1,30):
        data = luts[l-1]
        print(data)
        x = np.linspace(0,1, len(data))

        data_cumsum = np.cumsum(data[::-1])[::-1]
        normed = data_cumsum/data_cumsum[0]
        logdata = np.log10(normed)

        #fit line, only use finite entries (nonzero in nonlog)
        #params = curve_fit(func, x[normed_logdata>0],normed_logdata[normed_logdata>0])
        try:
            params = curve_fit(func, x[np.isfinite(logdata)],logdata[np.isfinite(logdata)])
            param_list.append(params[0])
            extrapolation = func(x, *params[0])

            if plot == True:
                plot_scores(eps, l, logdata, extrapolation)
            #extrapolation[normed_logdata>0] = normed_logdata[normed_logdata>0]
            luts[l-1] = extrapolation
        except:
            luts[l-1] = None
    return param_list
    #plot_params(param_list)


def plot_params(param_list):
    import seaborn as sns
    sns.set_style("darkgrid", {'font.sans-serif': ['Helvetica', 'Liberation Sans', 'Bitstream Vera Sans', 'sans-serif']})
    create_plot("Histogram", "Length", "slope and intercept ")
    ax = plt.subplot(111)

    for i, x in enumerate(param_list):
        ax.plot(i+1, x[0],  color = "b", marker = ".", label = r"$m_L$")
        ax.plot(i+1, x[1],  color = "g", marker = ".", label = r"$b_L$")

    handles, labels = ax.get_legend_handles_labels()

    ax.legend(handles = handles[0:2], labels = labels[0:2])
    plt.savefig("params for lengths.pdf")
    plt.close()

def main(args):
    settings = io.get_settings(args.dmrs)
    if args.lut is None:
        luts = None
        for _ in range(args.resampled ):
            #redistribute data and compute histograms
            lut = compute_methylation_distributions(args.classes, args.classfile, args.input, args.format, settings)
            lut = np.array(lut)
            if luts is None:
                luts = lut
            else:
                #add up all counts
                for i, subtable in enumerate(lut):
                    luts[i] += subtable
        np.save("lut.npy", luts)
    else:
        try:
            luts =  np.load(args.lut)
        except:
            print("Error loading look up table")
    extrapolate_and_convert_to_pValues(luts, settings["precision"], func= linear_func, plot = True)
    data = io.read_whole_dmrs(args.dmrs)
    compute_pvalues(data, luts, settings["precision"])
