from collections import OrderedDict
from itertools import tee
from pprint import pprint

import numpy as np
import h5py
import pandas as pd
import sys
from diffmer import io


def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)


def cut(cut, df):
    """trennt nach Distanz auf und schreibt die Daten raus"""

    akt_pos = df.index[0]
    for x, y in pairwise(df.index):
        dist = y-x
        if dist >= cut:
            # nicht in so kleine teilstücke zerscheneiden..
            if(df.loc[akt_pos:x, :].shape[0] > 5000):
                # df.loc[akt_pos:x, :].to_csv(outfolder+str(akt_pos) + \
                    #   "_"+str(x) +".csv", sep= "\t")
                yield akt_pos, x
                akt_pos = y
    # am Ende angekommen
    yield akt_pos, x


def compute_longest_piece(cut, df):
    """berechnet die Groesse des laengsten zusammenhaengenden Stuecks,
       wenn nach einer gegebenen bp-Distanz aufgetrennt wird

    cut: Distanz, nach der aufgetrennt wird

    df: Dataframe, der Index muss die cpg -positionen enthalten
    """

    longest = 0
    akt_pos = df.index[0]

    for x, y in pairwise(df.index):
        dist = y-x
        if dist >= cut:
            if(df.loc[akt_pos:x, :].shape[0] > longest):
                longest = df.loc[akt_pos:x, :].shape[0]
            akt_pos = y
            # am Ende angekommen
    if(df.loc[akt_pos:x, :].shape[0] > longest):
        longest = df.loc[akt_pos:y, :].shape[0]
    del df
    return longest


def compute_longest_pieces(data):
    zuordnung = dict()
    # distances = np.arange(100,210,10)
    distances = [1000]
    # initialisieren
    for d in distances:
        zuordnung[d] = 0

    for d in distances:
        a = compute_longest_piece(d, data)
        if a > zuordnung[d]:
            zuordnung[d] = a
            pprint(zuordnung)


def CpG_positions(chrom, folder):
    pos_f = h5py.File(folder+"index_hs37d5_Phix_Lambda.hdf5", "r")
    return pos_f[chrom]["cpg_positions"][:]


def read_melanome_methylation(chrom, folder):
    positions = CpG_positions(chrom, folder)

    f = h5py.File(folder+"19227_combi.hdf5", "r")
    values = f[chrom]["methylation"][:]

    methylated = np.sum(values[:, (0, 2)], axis=1)
    coverage = np.sum(values, axis=1)
    d = np.vstack((methylated, coverage))

    filename_s = ["30505_combi.hdf5", "20780_combi.hdf5", "21437_combi.hdf5"]
    for filename in filename_s:
        f = h5py.File(folder+filename, "r")
        values = f[chrom]["methylation"][:]
        methylated = np.sum(values[:, (0, 2)], axis=1)
        coverage = np.sum(values, axis=1)
        d = np.vstack((d, methylated, coverage))

    data = pd.DataFrame(d, columns=positions,
                        index=["M", "U+M", "M", "U+M", "M", "U+M", "M", "U+M"])
    return data


def melanome_methylation(folder, chrm=None):
    print("reading melanome methylation..")
    if chrm is None:
        for c in range(1, 23):
            chrom = str(c)
            frame = read_melanome_methylation(chrom, folder).transpose()
            case = frame.iloc[:, 0:4]
            control = frame.iloc[:, 4:8]
            yield frame, case, control, chrom
    else:
        print("single chromosome "+chrm)
        frame = read_melanome_methylation(chrm, folder).transpose()
        case = frame.iloc[:, 0:4]
        control = frame.iloc[:, 5:8]
        yield frame, case, control, chrm


def macaque_methylation(folder, chrm=None):
    print("reading macaque methylation..")
    if chrm is None:
        for c in range(1, 23):
            chrom = str(c)
            frame = read_macaque_methylation(chrom, folder)
            case = frame.iloc[:, [0, 1, 6, 7, 10, 11]]
            control = frame.iloc[:, [2, 3, 4, 5, 8, 9]]
            yield frame, case, control, chrom
    else:
        # print("single chromosome "+chrm)
        frame = read_macaque_methylation(chrm, folder)
        case = frame.iloc[:, [0, 1, 6, 7, 10, 11]]
        control = frame.iloc[:, [2, 3, 4, 5, 8, 9]]
        yield frame, case, control, chrm


def read_macaque_methylation(chrom, folder):
    relevant_cols = [0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
    frame = pd.read_csv(folder+"GSE34128_cpgs_methyl_coverage_macaqueonly.bed",
                        delim_whitespace=True, header=None,  index_col=[0, 1],
                        usecols=relevant_cols)
    frame = frame.loc["chr"+chrom]
    frame.columns = ["M", "U+M", "M", "U+M",
                     "M", "U+M", "M", "U+M",
                     "M", "U+M", "M", "U+M"]
    return frame


def write_RadMeth_file(path_to_data, path_to_output):
    with open(path_to_output+"proportion_table2.txt", "a") as f:
        f.write("case_a\tcase_b\tcontrol_a\tcontrol_b\n")
        for c in range(12, 23):
            chrom = str(c)
            frame = read_melanome_methylation(chrom, path_to_data).transpose()
            frame.columns = np.arange(frame.shape[1])
            frame = frame[[1, 0, 3, 2, 5, 4, 7, 6]]
            new_index = ["chr" + chrom + ":"+str(pos) + ":" + str(pos+1)
                         for pos in frame.index]
            frame.index = new_index
            frame.to_csv(f, mode="a", header=False, sep="\t")


def filter_BSmooth(path_to_data, path_to_output):
    data = pd.read_csv(path_to_data, delim_whitespace=True, index_col=None)
    # data = data.sort_index()
    data = data[[3, 4, 5, 1, 0, 2]].sort(["Chr", "Start"])
    data.index = np.arange(len(data.index))
    data.to_csv(path_to_output+"sorted_dmr.csv", sep="\t")


def rewritehdf5(folder="/home/hesse/Downloads/"):
    # positions = CpG_positions(chrom, folder)
    newf = h5py.File("aderhaut.hdf5", "w")
    filename_s = ["30505_combi.hdf5",
                  "20780_combi.hdf5",
                  "21437_combi.hdf5",
                  "19227_combi.hdf5"]
    for filename in filename_s:
        sample_name = filename[:5]
        grp = newf.create_group(sample_name)

        f = h5py.File(folder+filename, "r")

        # for name in f:
        for c in range(1, 23):
            chrom = str(c)
            a = grp.create_group(chrom)
            values = f[chrom]["methylation"][:]
            methylated = np.sum(values[:, (0, 2)], axis=1)
            coverage = np.sum(values, axis=1)
            d = np.vstack((methylated, coverage))
            np.transpose(d)
            a.create_dataset("methylation", data=d)
    grp = newf.create_group("positions")
    for c in range(1, 23):
        chrom = str(c)
        positions = CpG_positions(chrom, folder)
        grp.create_dataset(chrom, data=positions)


def converthdf5(folder):
    for c in range(1, 23):
        chrom = str(c)
        for filename in ["30505_combi.hdf5"]:

            f = h5py.File(folder+filename, "r")

            values = f[chrom]["methylation"][:]
            methylated = np.sum(values[:, (0, 2)], axis=1)
            coverage = np.sum(values, axis=1)
            d = np.vstack((methylated, coverage))
        fnames = ["19227_combi.hdf5", "20780_combi.hdf5", "21437_combi.hdf5"]
        for filename in fnames:
            f = h5py.File(folder+filename, "r")
            values = f[chrom]["methylation"][:]
            methylated = np.sum(values[:, (0, 2)], axis=1)
            coverage = np.sum(values, axis=1)
            d = np.vstack((d, methylated, coverage))
        d = d.transpose()
        pos_f = h5py.File(folder+"index_hs37d5_Phix_Lambda.hdf5", "r")
        positions = pos_f[chrom]["cpg_positions"][:]
        end_pos = positions+1
        index = [["chr" + chrom for _ in range(len(positions))],
                 positions, end_pos]
        akt_df = pd.DataFrame(data=d, index=index)
        akt_df.to_csv(sys.stdout, sep="\t", header=None)


def merge_bed(folder, filenames):
    """reads several bedfiles (one per sample) and merges them into one"""
    # step1: trim all unused infos
    df = pd.read_csv(folder + filenames[0], delim_whitespace=True,
                     index_col=[0, 1, 2], header=None).iloc[:, 0]
    chromosomes = list(OrderedDict.fromkeys(df.index.get_level_values(0)))
    print(chromosomes)
    for chrom in chromosomes:
        frames = []
        for f in filenames:
            print(f)
            df = pd.read_csv(folder + f, delim_whitespace=True,
                             index_col=[0, 1, 2],
                             header=None).iloc[:, 0].loc[chrom]
            new = [x.strip("'").split("/") for x in df.values]

            index = [[chrom for _ in range(len(df.index.get_level_values(0)))],
                     df.index.get_level_values(0),
                     df.index.get_level_values(1)]
            new_df = pd.DataFrame(index=index, data=new, dtype=int)
            frames.append(new_df)
            del df
            del new
            del new_df

        df = pd.concat(frames, axis=1)
        # print(df)
        df = df.fillna(0)
        df = df.astype(int)
        df.to_csv(folder+"input.bed", mode="a", sep="\t", header=None)


def read_index(ix_file, chrom):
    """"""
    chrom = chrom.strip("chr")

    if "gpc_positions" in ix_file[chrom]:
        return ix_file[chrom]["gpc_positions"][:]
    else:
        return ix_file[chrom]["cpg_positions"][:]


def exists_region(ix_file, chrom, start, stop):
    ix = read_index(ix_file, chrom)
    temp = ix[np.where(ix >= start)]
    sliced = temp[temp <= stop]

    if sliced is None or len(sliced) == 0:
        return False
    else:
        return True


def getOverlapPercent(a, b):
    """How much percentage of a are in b?"""
    overlap = max(0, min(a[-1], b[-1]) - max(a[0], b[0]))
    return overlap/(a[-1]-a[0])


def nearest_neighbours(dmr, B):
    """computes to a given dmr = (chrom, start, stop) the nearest 2 regions
       in other_dmrs, as well as its distance"""

    chrom, start, stop = dmr
    chromB, startB, stopB, *_ = next(B)
    left = None
    right = None
    left_region = ""
    right_region = ""
    try:
        while (chromB < chrom):
            chromB, startB, stopB, *_ = next(B)
        while (chrom == chromB):
            if stopB < start:   # left neighbour, assume sorted order
                # left = (startB, stopB, start-stopB)
                left = start-stopB
                left_region = (startB, stopB)
            if stop < startB:  # right neighbour
                # right = (startB, stopB, startB-stop)
                right = startB-stop
                right_region = (startB, stopB)
                return left, right, left_region, right_region
            chromB, startB, stopB, *_ = next(B)
            # same chromosome, get 2 nearest dmrs
        return left, right, left_region, right_region
    except StopIteration:
        # no match found
        return left, right, left_region, right_region


def intersection(A, B, index_A, index_B):
    """computes the overlap for each region in one bedfile with another.
    If the overlap is zero, """
    backup = list(B)  # save to search multiple times
    B = iter(backup)
    chromB, startB, stopB, *_ = next(B)
    chrom, start, stop, other = next(A)
    left = None
    right = None

    # TODO: A might be in 2 or more DMRs of B
    try:
        while(True):
            if chrom < chromB:
                left, right, left_region, right_region = \
                    nearest_neighbours((chrom, start, stop), iter(backup))
                # chromosome A not in solution of B
                if exists_region(index_B, chrom, start, stop):
                    print(chrom, start, stop, "0%",
                          left, right, left_region, right_region, *other)
                else:
                    print(chrom, start, stop, "N/A",
                          left, right, left_region, right_region, *other)
                chrom, start, stop, other = next(A)
            elif chromB < chrom:
                chromB, startB, stopB, *_ = next(B)
            else:

                # test for same chromosome
                if stop < startB:  # A is not in B
                    left, right, left_region, right_region = \
                        nearest_neighbours((chrom, start, stop), iter(backup))
                    if exists_region(index_B, chrom, start, stop):
                        print(chrom, start, stop, "0%",
                              left, right, left_region, right_region, *other)
                    else:
                        print(chrom, start, stop, "N/A", left, right,
                              left_region, right_region, *other)
                    chrom, start, stop, other = next(A)
                elif start > stopB:  # get the next B
                    chromB, startB, stopB, *_ = next(B)
                else:  # there must be an overlap
                    # get an updated version of the indices and compare them

                    ixA = read_index(index_A, chrom)
                    tempA = ixA[np.where(ixA >= start)]
                    slicedA = tempA[tempA <= stop]

                    ixB = read_index(index_B, chromB)
                    tempB = ixB[np.where(ixB >= startB)]
                    slicedB = tempB[tempB <= stopB]

                    percent = getOverlapPercent(slicedA, slicedB)

                    print(chrom, start, stop, "{:.2f}%".
                          format(percent*100), 0, 0, *other)
                    # get next
                    chrom, start, stop, other = next(A)

                    chromB, startB, stopB, *_ = next(B)

    except StopIteration:
        # test which one is exhasuted:
        for chrom, start, stop, other in A:
            if exists_region(index_B, chrom, start, stop):
                print(chrom, start, stop, "0%", "Test", *other)
            else:
                print(chrom, start, stop, "N/A", *other)


def custom_min(a, b):
    if a == "None":
        return int(b)
    if b == "None":
        return int(a)
    return min(int(a), int(b))


def sort_regions(filename):
    all_dmrs = []
    with open(filename, "r") as f:
        for line in f:
            if line.startswith("#"):
                continue
            chrom, start, stop, percent, left, right, *others = line.split(" ")
            others[-1] = others[-1].strip("\n")
            temp = [chrom, start, stop, percent, left, right]
            temp.extend(others)
            all_dmrs.append(temp)

    result = sorted(all_dmrs, key=lambda x: custom_min(x[4], x[5]))
    for x in result:
        print(*x)


if __name__ == "__main__":
    A = io.read_whole_dmrs("/scratch/Monozyten/WGBS/diffmer/dmrs_WGBS_small.bed")
    B = io.read_whole_dmrs("/scratch/Monozyten/NomeSeq/diffmer/dmrs_singleweights_4aspace.bed")
    index_A = h5py.File("/scratch/Monozyten/WGBS/data/index_hs37d5_Phix_Lambda.hdf5", "r")
    index_B = h5py.File("/scratch/Monozyten/NomeSeq/index_hs37d5_Phix_Lambda_nome.hdf5", "r")

    # intersection(A,B,index_A, index_B)
    sort_regions("/scratch/Monozyten/NomeSeq/diffmer/nearest.bed")
